# encoding: utf-8

class Node(object):

    def __init__(self, key = None, leftchild = None, rightchild = None):
        self.key = key
        self.node_number = 1
        self.leftchild = leftchild
        self.rightchild = rightchild

    def get_left_child(self):
        return self.leftchild

    def get_right_child(self):
        return self.rightchild

    def set_left_child(self, node):
        self.leftchild = node
        self.leftchild.node_number = self.node_number * 2

    def set_right_child(self, node):
        self.rightchild = node
        self.rightchild.node_number = self.node_number * 2 + 1

    def get_key(self):
        return self.key


class Btree(object):

    def __init__(self, root = None):
        self.root = root

    def get_root(self):
        return self.root

    def show(self):
        self.show_node(self.root)

    def show_node(self, node):
        print "Вершина номер: %2s, значение: %2s, левый ребёнок: %2s, правый ребёнок: %2s" \
            % (node.node_number, node.get_key(), node.get_left_child().get_key() if node.get_left_child() != None \
                else "-", node.get_right_child().get_key() if node.get_right_child() != None else "-")
        if node.get_left_child() != None:
            self.show_node(node.get_left_child())
        if node.get_right_child() != None:
            self.show_node(node.get_right_child())


def insert_new_node(currentnode, newnode):
    if newnode.get_key() < currentnode.get_key():
        if currentnode.get_left_child() == None:
            currentnode.set_left_child(newnode)
        else:
            insert_new_node(currentnode.get_left_child(), newnode)
    else:
        if currentnode.get_right_child() == None:
            currentnode.set_right_child(newnode)
        else:
            insert_new_node(currentnode.get_right_child(), newnode)


def construct_tree(depth, *keyvalues):
    t = Btree(Node(keyvalues[0]))
    for keyindex in xrange(1, len(keyvalues)):
        n = Node(keyvalues[keyindex])
        insert_new_node(t.get_root(), n)
    return t


tree = construct_tree(0, 14, 7, 9, 45, 34, 2, 27, 5)
tree.show()
