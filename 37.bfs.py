# BFS (Breadth-first search) algorythm

class Vertex(object):

    def __init__(self, parent = None, color = "white"):
        self.weight = 999999999
        self.parent = parent
        self.color = color


class Edge(object):

    def __init__(self, graph, v1_ind, v2_ind, weight):
        self.v1 = graph.vertexes[v1_ind]
        self.v2 = graph.vertexes[v2_ind]
        self.weight = weight


class Graph(object):

    def __init__(self, vertex):
        self.vertexes = [vertex]
        vertex.color = "gray"
        vertex.weight = 0
        self.edges = []
        self.queue = [vertex]

    def __add__(self, obj):
        if type(obj) == Vertex:
            self.vertexes.append(obj)
        elif type(obj) == Edge:
            self.edges.append(obj)
        return self

    def show_vertexes(self):
        for v in self.vertexes:
            print "Vertex number", self.vertexes.index(v), ", weight:", v.weight, \
                ", parent:", self.vertexes.index(v.parent) if v.parent != None \
                    else "None", " color:", v.color

    def show_edges(self):
        for e in self.edges:
            print "Neighboards: vertex1", self.vertexes.index(e.v1), ", vertex2", \
                self.vertexes.index(e.v2), ", weight:", e.weight


g = Graph(Vertex())

g += Vertex()
g += Vertex()
g += Vertex()
g += Vertex()
g += Vertex()
g += Edge(g, 0, 1, 1)
g += Edge(g, 0, 2, 1)
g += Edge(g, 1, 3, 1)
g += Edge(g, 1, 4, 1)
g += Edge(g, 2, 4, 1)
g += Edge(g, 2, 5, 1)

g.show_vertexes()
g.show_edges()

while g.queue != []:
    u = g.queue.pop(0)
    for e in g.edges:
        if e.v1 == u:
            if e.v2.color == "white":
                e.v2.color = "grey"
                e.v2.weight = e.v1.weight + 1
                e.v2.parent = u
                g.queue.append(e.v2)
    u.color = "black"

print "Graph after BFS:"
g.show_vertexes()

