import os

mo = {"name": "Suzan", "age": 41, "child": None}
son = {"name": "Alex", "age": 8, "parent": mo}
mo["child"] = son
for a in mo:
    print "key:", a, "value", mo[a]
