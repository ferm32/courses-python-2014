# coding: utf-8
class Auto:
    maxSpeed = 130 # км/час
    model = 'Lada'
    speed = 15 # м/с
    def drive(self, t):
	return t * self.speed

my_vaz2110 = Auto()
print my_vaz2110.drive(50)
#class Benzine:

class Benzine(Auto):
    kpd = 0.95

my_second_auto = Benzine()
print my_second_auto.drive(50)*my_second_auto.kpd

class Auto2(Auto):
    benzine_type = 95
    volume = 30
    outage = 7.5

    def print_table(self):
	for oil in [0.92, 0.95, 0.98]:
	    if (oil * self.outage) <= self.volume:
		print "{0}, {1}, {2}".format(oil, self.outage, 100000/self.speed/oil)

my_third_auto = Auto2()
my_third_auto.print_table()
