# encoding: utf-8
class Node(object):
    def __init__(self, key = None, leftchild = None, rightchild = None):
        self.key = key
        self.leftchild = leftchild
        self.rightchild = rightchild
    def getleftchild(self):
        return self.leftchild
    def getrightchild(self):
        return self.rightchild
    def setleftchild(self, node):
        self.leftchild = node
    def setrightchild(self, node):
        self.rightchild = node
    def getkey(self):
        return self.key

class Btree(object):
    def __init__(self, root = None):
        self.root = root
    def getroot(self):
        return self.root
    def show(self):
        self.shownode(self.root)
    def shownode(self, node):
#        print "Вершина:", node.getkey(), "левый ребёнок:", node.getleftchild().getkey() if node.getleftchild() != None else "-", " правый ребёнок:", node.getrightchild().getkey() if node.getrightchild() != None else "-"
        print "Вершина: %2s, левый ребёнок: %2s, правый ребёнок: %2s" % (node.getkey(), node.getleftchild().getkey() if node.getleftchild() != None else "-", node.getrightchild().getkey() if node.getrightchild() != None else "-")
        if node.getleftchild() != None:
            self.shownode(node.getleftchild())
        if node.getrightchild() != None:
            self.shownode(node.getrightchild())

def insertnewnode(currentnode, newnode):
    if newnode.getkey() < currentnode.getkey():
        if currentnode.getleftchild() == None:
            currentnode.setleftchild(newnode)
        else:
            insertnewnode(currentnode.getleftchild(), newnode)
    else:
        if currentnode.getrightchild() == None:
            currentnode.setrightchild(newnode)
        else:
            insertnewnode(currentnode.getrightchild(), newnode)

def constructtree(depth, *keyvalues):
    t = Btree(Node(keyvalues[0]))
    for keyindex in xrange(1, len(keyvalues)):
        n = Node(keyvalues[keyindex])
        insertnewnode(t.getroot(), n)
    return t

tree = constructtree(0, 14, 7, 9, 45, 34, 2, 27, 5)
tree.show()
