# encoding: utf-8
# Сортировка слиянием.

def merge(l1, l2):
    result = []
    print "merge(", l1, ",", l2, ") is called.",
    while l1 != [] or l2 != []:
        if l1 == []:             # В первом списке уже ничего нет
            result = result + l2
            l2 = []
        elif l2 == []:           # Во втором списке уже ничего нет
            result = result + l1
            l1 = []
        else:
            if l1[0] > l2[0]:
                result = result + [l2.pop(0)]
            else:
                result = result + [l1.pop(0)]
    print " Result is: %2s" % (result)
    return result


def decomposition(l):
    list_len = len(l)
    if list_len > 1:
        sublist1 = l[:(list_len / 2)]
        sublist2 = l[(list_len / 2):]
        print "decomposition(%2s) is called. Result is: %2s %2s" % \
            (l, sublist1, sublist2)
        return merge(decomposition(sublist1), decomposition(sublist2))
    else:
        return l


l = [1,6,3,6,7,7,3,5,7,9,8,4,6,7,3,6,0,23,53,22,14,5,66,203,23,-3,44,22.99, \
    2.3, -100]
res = decomposition(l)
print "Общий результат: ", res
