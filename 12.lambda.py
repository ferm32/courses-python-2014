#7 if a else 8
func = lambda action, operand1, operand2: operand1 + operand2 if action=="plus" else operand1 - operand2 if action=="minus" else \
operand1 * operand2 if action=="multiply" else operand1 / operand2 if action=="divide" else 0
print func("plus", 5, 7)
print func("minus", 7, 5)
print func("divide", 10, 3)

