# encoding: UTF-8
import time

class Date(object):
#    value = (17, 3, 2014) # Так можно было бы написать, но мы идём другим путём
    def __init__(self, val):
	self.value = val
    def now(self):
	t = time.localtime()
	return Date(t)

class EuroDate(Date):
    def __str__(self):
	return "%d-%d-%d" % (
	    self.value.tm_year,
	    self.value.tm_mon,
	    self.value.tm_mday
	)

val = time.localtime()
d1 = Date(val)
d2 = d1.now()
print d1
print d2
print d1.now
print EuroDate(val)

ed1 = EuroDate(val)
#ed2 = EuroDate.now()
print "Eurodate"
print ed1
#print ed1.now()
#print ed2
