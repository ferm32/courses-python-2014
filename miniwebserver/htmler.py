# encoding: utf-8
import menu
def htmler(page, list):

    def students_page(list):
        stud_page = '''
        <HTML>
        <HEAD>
	    <meta charset="utf-8">
            <TITLE>Список студентов команды</Title>
        </HEAD>

        <BODY>
'''+menu.menu+'''
        <H2>Список студентов команды</H2>
	<kbd> {0} </kbd>
        </BODY>
        </HTML>
        '''
        command = []
        l=0
        while l < len(list):
            command.append("%s: %s" % (list[l][0], list[l][1]))
            l+=1
        st_text = '<br/>'.join(command)
        return stud_page.format(st_text)

    def homeworks_page(list):
        hw_page = '''
        <HTML>
        <HEAD>
	    <meta charset="utf-8">
            <TITLE>Работы студентов команды</Title>
        </HEAD>
        <BODY>
'''+menu.menu+'''
        <H2>Работы студентов команды</H2>
        <kbd>{0}</kbd>
        </BODY>
        </HTML>
        '''
        works = []
        hw=0
        while hw < len(list):
            works.append("%s: %s, %s" % (list[hw][0], list[hw][1], list[hw][2]))
            hw+=1
        hw_text = '<br/>'.join(works)
        return hw_page.format(hw_text)

    def lessons_page(list):
        less_page = '''
        <HTML>
        <HEAD>
	    <meta charset="utf-8">
            <TITLE>Лекции</Title>
        </HEAD>

        <BODY>
'''+menu.menu+'''
        <H2>Лекции</H2>
	<kbd>{0}</kbd>
        </BODY>
        </HTML>
        '''
        less = []
        l=0
        while l < len(list):
            less.append("%s: %s" % (list[l][0], list[l][1]))
            l+=1
        less_text = '<br/>'.join(less)
        return less_page.format(less_text)

    if page=='students':
        return students_page(list)
    elif page=='lessons':
        return lessons_page(list)
    elif page=='homework':
        return homeworks_page(list)
    else:
	return "Error 404"
