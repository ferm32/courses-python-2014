def sqler(sss):
    import sqlite3


    def connectToDB(sqlite_file):
        """ Make connection to an SQLite database file """
        conn = sqlite3.connect(sqlite_file)
        c = conn.cursor()

        return conn, c


    def createDB():
        sqlite_file = '/home/vitaly/courses-python-2014/miniwebserver/my_first_db.sqlite'    # name of the sqlite database file

        # Connecting to the database file
        conn, c = connectToDB(sqlite_file)

        print 'DB my_first_db.sqlite wa created!'

        # Creating a new SQLite table with 1 column
        c.execute('CREATE TABLE students (stud_id INTEGER, stud_name TEXT)')
        c.execute('CREATE TABLE lessons (less_id INTEGER, less_name TEXT)')
        c.execute('CREATE TABLE homework (hw_id INTEGER, less_id INTEGER, hw_name TEXT)')

        conn.commit()

        return sqlite_file


    def insertData(sqlite_file):
        conn, c = connectToDB(sqlite_file)
        c.execute("INSERT INTO students (stud_id, stud_name) VALUES (1, 'Kseniya')")
        c.execute("INSERT INTO students (stud_id, stud_name) VALUES (2, 'Vitaly')")
        c.execute("INSERT INTO students (stud_id, stud_name) VALUES (3, 'Iliya')")
        c.execute("INSERT INTO students (stud_id, stud_name) VALUES (4, 'Alex')")
        c.execute("INSERT INTO students (stud_id, stud_name) VALUES (5, 'Pavel')")

        print 'data was inserted into table STUDENTS'

        c.execute("INSERT INTO lessons (less_id, less_name) VALUES (1, 'Data type')")
        c.execute("INSERT INTO lessons (less_id, less_name) VALUES (2, 'Generators')")
        c.execute("INSERT INTO lessons (less_id, less_name) VALUES (3, 'Comprehension')")
        c.execute("INSERT INTO lessons (less_id, less_name) VALUES (4, 'OOP')")

        print 'data was inserted into table LESSONS'

        c.execute("INSERT INTO homework (hw_id, less_id, hw_name) VALUES (1, 1, 'create lists, dictionary, numbers')")
        c.execute("INSERT INTO homework (hw_id, less_id, hw_name) VALUES (2, 2, 'create generator')")
        c.execute("INSERT INTO homework (hw_id, less_id, hw_name) VALUES (3, 3, 'create comprehension')")
        c.execute("INSERT INTO homework (hw_id, less_id, hw_name) VALUES (4, 4, 'create test class')")

        print 'data was inserted into table HOMEWORK'

        conn.commit()


    def select(sqlite_file, table_name):
        conn, c = connectToDB(sqlite_file)
        c.execute('SELECT * FROM {tn} ORDER BY 1'.format(tn=table_name))
        all_rows = c.fetchall()
        return all_rows

    # if __name__ == 'name':
    # db_name = createDB()
    # insertData(db_name)
    db_name = '/home/vitaly/courses-python-2014/miniwebserver/my_first_db.sqlite'
    xxxx=select(db_name,sss)
#    createDB()
#    db_name = createDB()
#    insertData(db_name)
    return xxxx

#sqler('students')