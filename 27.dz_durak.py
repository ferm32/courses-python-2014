# coding: utf-8
# Прототип карточной игры Дурак v 0.2
# Использованы: свойства, статический метод, метод класса, метакласс, декоратор.
# На данный момент игра умеет раздавать и показывать состояние колоды и карт на руках,
# определять порядок осуществления ходов игроками.
import random, sys, codecs, locale

sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)

class Party(object):
    trump = ""
    ranks = ("2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A")
    suits = (u"♦", u"♥", u"♠", u"♣")
    def __init__(self, *player_names):
        self.deck = Deck()
        self.deck.mix()
        self.players = []
        for p_n in player_names:
            self.players.append(Player(p_n))
    def showtrump(func):
        def f(self):
            func(self)
            print u"После раздачи карт определился козырь: "+self.trump
        return f
    @showtrump      # После первой раздачи карт надо вывести на экран козырь
    def firstdeal(self):
        'Первая раздача - по 6 карт кажому, определение козыря, определение порядка ходов'
        for i in xrange(6):
            for p in self.players:
                p.getnewcard(self.deck)
        self.deck.cards.insert(0, self.deck.cards.pop())
        self.trump = self.deck.cards[0].suit
        # Переставить игроков циклически таким образом, чтобы первым ходил тот, у кого наименьший козырь
        # Если козырей ни у кого нет, игроки начинают в том порядке, как были объявлены в партии
        # 25 - заведомо наибольший индекс среди всех возможных старшинств
        lowest_trump_indexes = [Party.ranks.index(p.lowestcard(self.trump)) if p.lowestcard(self.trump) != None else 25 for p in self.players]
        if min(lowest_trump_indexes) != 25:   # Хотя бы у одного игрока есть козырь
            for i in xrange(lowest_trump_indexes.index(min(lowest_trump_indexes))):
                self.players.append(self.players.pop(0))
    @staticmethod
    def showstring():
        print u"Обзор состояния игры:"
    def show(self):
        'Показать общую диспозицию'
        self.deck.show()
        for p in self.players:
            p.show()

class Card(object):
    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit
    @property
    def suit(self):
        return self.__var
    @suit.setter
    def suit(self, value):
        self.__var = value
    def suit_is(self,suit):
        return self.suit == suit
    def is_trump(self):
        return self.suit == Party.trump
        
class Meta(type):
    def __init__(cls, name, bases, dct):
        def __lt__(self, o):
            return Party.ranks.index(self.rank) < Party.ranks.index(o.rank) if self.suit == o.suit \
                else True if o.suit == Party.trump \
                else False
        cls.__lt__ = __lt__
     
class OneCard(Card):
    __metaclass__ = Meta
    @classmethod
    def showattributes(cls):
        print u"Класс OneClass получает из метакласса метод __lt__, проверяем OneCard.__dict__: ", cls.__dict__

class Deck(object):
    def __init__(self):
        self.cards = []
        for s in Party.suits:
            for r in Party.ranks:
                self.cards.append(OneCard(r, s))
    def show(self):
        print "Deck:",
        for c in self.cards:
            print c.rank+c.suit,
        print ""
    def mix(self):
        '''Перемешивание колоды.  Сначала выбирается случайная карта из всей колоды,
извлекается и помещается в конец колоды. Затем извлекается случайная карта
из всей колоды, кроме послденей карты и помещается в конец колоды.
Подобная процедура повторяется, пока вся колода не окажется перемешанной.'''
        for i in xrange(len(self.cards), 1, -1):
            self.cards.append(self.cards.pop(self.cards.index(random.choice(self.cards[0:i]))))
    def dealonecard(self):
        return self.cards.pop()
            
class Player(object):
    def __init__(self, name):
        self.name = name    # Имя игрока
        self.on_hand = []            # Карты на руках игрока
    def getnewcard(self, deck):
        self.on_hand.append(deck.dealonecard())
    def lowestcard(self, suit):
        suit_cards = [Party.ranks.index(c.rank) for c in self.on_hand if c.suit_is(suit)]
        if suit_cards != []:
            return Party.ranks[min(suit_cards)]
        else:
            return None
    def show(self):
        print self.name+":",
        for c in self.on_hand:
            print c.rank+c.suit,
        for s in Party.suits:
            print u"Наименьшая карта масти "+s+":", self.lowestcard(s),
        print ""

OneCard.showattributes()    # Демонстрация работы метода класса, а заодно и метакласса Meta
p = Party("Ivan", "Petr")
Party.showstring()              # Демонстрация работы статического метода
p.show()
p.firstdeal()
Party.showstring()              # Демонстрация работы статического метода
p.show()
