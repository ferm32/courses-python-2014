# encoding: utf-8
def translate(word, maxnum=0, *categories):
# = ["nouns", "verbs", "adjectives"]
    #Доделать дома
    dict = (
	    {"english_word":"sun",
		"transcr": ("s^n",),
		"nouns": (
			    "Солнце",
			    "солнце",
			    "солнечный свет",
			    "день (поэт.)"
			),
		"verbs": (),
		"adjectives":(
			    "греться на солнце",
			    "сушить, греть на солнце",
			    "выставлять на солнце",
			    "светить как солнце"
			    )
		},
		{"english_word":"like",
		"transcr": ("l^ke",),
		"nouns": (
			    "нечто подобное, равное, одинаковое",
			    "подобный человек"
			),
		"adjectives": (
			    "аналогичный, подобный, похожий, сходный",
			    "идентичный, одинаковый, равный, тождественный",
			    "возможный; вероятный"
			),
		"verbs": (
			    "любить, нравиться",
			    "хотеть, желать",
			    "предпочитать"
			    )
		}
	)
    categories_list = []
    if categories == ():
        categories_list = ["transcr", "nouns", "verbs", "adjectives"]
    else:
	for c in categories:
	    categories_list.append(c)
    print categories_list

    def limited_output(w, category, limit):
	if category in categories_list:
	    print ("транскрипция:" if category=="transcr" else "существительное" if category=="nouns" else "прилагательное" if category=="adjectives" else "глагол" if category=="verbs" else "")
	    counter = 0
	    for substr in w[category]:
		print "%i. %s" % (counter+1, substr)
		counter += 1
		if counter>=limit>0:
		    break

    for w in dict:
	if w["english_word"] == word:
		limited_output(w, "transcr", maxnum)
		limited_output(w, "nouns", maxnum)
		limited_output(w, "verbs", maxnum)
		limited_output(w, "adjectives", maxnum)
#		if "nouns" in categories_list:
#		    print "nouns: ", w["nouns"]
#		if "verbs" in categories_list:
#		    print "verbs: ",w["verbs"]
#		if "adjectives" in categories_list:
#		    print "adjectives: ", w["adjectives"]
		
#		print "%s" % w

translate("like")
translate("sun", 3)
translate("sun", 2, "transcr", "adjectives")
