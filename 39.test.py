class Node(object):

    def __init__(self):
        self.childs = []

    def add_child(self, child_class):
        self.childs.append(child_class())
        return self.childs[-1]

    def walk_and_summ(self, curr_summ):
        self.summ = self.execute(curr_summ)
        for n in self.childs:
            self.summ = n.walk_and_summ(self.summ)
        return self.summ


class Client(Node):
    def execute(self, currentsum):
        return currentsum


class Coffee(Node):
    def execute(self, currentsum):
        return currentsum + 50


class Pizza(Node):
    def execute(self, currentsum):
        return currentsum + 300


class Tree(Client, Coffee, Pizza):

    def __init__(self, root_class):
        self.summ = 0
        root_class.__init__(self)


root = Tree(Client)
node1 = root.add_child(Pizza)
node2 = root.add_child(Coffee)
node3 = node1.add_child(Pizza)

print root.walk_and_summ(0)
