# encoding: utf-8
# Реализация объекта стека
class Stack(object):
    def __init__(self, maxsize = 3):
        _currentposition = 0
        self.stack = [maxsize, _currentposition]
    def push(self, value):
        if self.stack[0] > self.stack[1]:
            self.stack.append(value)
            self.stack[1] += 1
        else:
            print "Stack Overflow!"
    def pop(self):
        if self.stack[1] > 0:
            self.stack[1] -= 1
            return self.stack.pop()
        else:
            print "Stack underflow!"
            return None
    def show(self):
        for i in xrange(0, self.stack[1]+2):
            print self.stack[i],
        print ""

s = Stack(2)
s.show()
s.push(1)
s.show()
s.push(2)
s.show()
s.push(3)
s.show()
print "s.pop() result:", s.pop()
s.show()
print "s.pop() result:", s.pop()
s.show()
print "s.pop() result:", s.pop()
s.show()
