# encoding: utf-8
# Пример реализации объекта очереди

# Пример экземпляра q класса Queue (сейчас в очереди 3 элемента - x, y, z):
# q.queue:
#<----------------- голова                хвост ----------------->
# +-------+-----------+------------+---+---+---+---+---+---+---+-----------
# |   0   |     1     |     2      |  с а м а   о ч е р е д ь  |
# |З а г о л о в о к  о ч е р е д и| 0 | 1 | 2 | 3 | 4 | 5 | 6 |
# +-------+-----------+------------+---+---+---+---+---+---+---+-----------
# |maxsize| head index| tail index | - | x | y | z | - | - | - |
# |  =7   |    =1     |     =3     | - | 7 | 3 | 8 | - | - | - |
# +-------+-----------+------------+---+---+---+---+---+---+---+-----------
#
#                                        ^ положение головы (1)
#
#                                                ^ положение хвоста (3)
#
# Новые элементы очереди помещаются в хвост, извлекаются из головы
# Очередь закольцована
from random import choice, randint

class Queue(object):
    def __init__(self, maxsize = 3):
# Голова и хвост очереди
# Голова слева, хвост справа
# Т.е. заполнение соответствует росту индекса хвоста
# Хвост в состоянии пустой очереди условно "забегает" на 1 позицию вперёд головы
# так как голова слева, хвост справа, то положение хвоста = положение головы - 1,
# для закольцованной очереди это будет 0 - 1 = -1 ---> послдений элемент очереди
# Это позволит при помещении первого элемента совместить голову и хвост
# Первоначальное заполнение идёт в порядке 0, 1, 2 и т. д., потом снова с 0
        _head_index = 0
        _tail_index = maxsize - 1
        self.queue = [maxsize, _head_index, _tail_index]
        self.current_length = 0
        for i in xrange(maxsize):
            self.queue.append(None)
        print "Создан объект очереди с максимальной длиной:", maxsize
    def push(self, value):
        print "В очередь поступило новое число:", value
        _header_maxsize_index = 0
        _header_head_index = 1
        _header_tail_index = 2
        _header_length = 3
        def overflow_error():
            print "Error! Queue overflow."
        def update_queue(index, value):
            self.queue[index + _header_length] = value
            self.queue[_header_tail_index] = index
            self.current_length += 1
        if self.queue[_header_tail_index] < self.queue[_header_maxsize_index] - 1:
            if (self.queue[_header_tail_index] + 1 != self.queue[_header_head_index]) | (self.current_length == 0):
                # Хвост очереди пока не "догнал" голову
                _new_tail_index = self.queue[_header_tail_index] + 1
                update_queue(_new_tail_index, value)
            else:
                overflow_error()
        else:
            if (self.queue[_header_head_index] != 0) | (self.current_length == 0):
                _new_tail_index = 0
                update_queue(_new_tail_index, value)
            else:
                overflow_error()
    def pop(self):
        _header_maxsize_index = 0
        _header_head_index = 1
        _header_tail_index = 2
        _header_length = 3
        def underflow_error():
            print "Error! Queue underflow."
        if self.current_length == 0:
            underflow_error()
        else:
            self.current_length -= 1
            _result = self.queue[self.queue[_header_head_index] + _header_length]
            self.queue[self.queue[_header_head_index] + _header_length] = None
            if self.queue[_header_head_index] < self.queue[_header_maxsize_index] - 1:
                self.queue[_header_head_index] += 1
            else:
                self.queue[_header_head_index] = 0
            return _result
    def show(self):
        print "[",
        for i in xrange(3, self.queue[0]+3):
            print "%4s" % self.queue[i],
        print "]",
        print  "     Положение головы:", self.queue[1], "положение хвоста:", self.queue[2], "текущая длина:", self.current_length

q = Queue(3)
q.show()
for i in xrange(15):
    if choice((0,1)) == 0:
        q.push(randint(0, 99))
    else:
        print "q.pop() вернул:", q.pop()
    q.show()

