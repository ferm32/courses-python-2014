# decoding: UTF-8
class C(object):

#    def __init__

    @property
    def x(self):
	return self.__x

    @x.setter
    def x(self, x):
	self.__x = x ** 2

    @x.deleter
    def	x(self):
	del self.__x

c = C()
c.x = 7
print c.x
del c.x

# Выдаст исключение, поэтому комментируем:
#print c.x
c.x =4
print c.x
