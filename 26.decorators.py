# decoding: UTF-8
# Декоратора для Auto
def withmodel (cls):
    if hasattr(cls, "model"):
        return(cls)
    class New(cls):
        model = "NoName"
    return New

@withmodel	
class Auto:
    maxspeed = 130	# км/ч
    model = "Lada"

print Auto.model

