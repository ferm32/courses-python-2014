# Не закончено, нуждается в отладке
def dec(cls):
    for a in cls.__dict__:
        meth = cls.__dict__[a]
        if "__call__" in dir(a):
            def func(*args):
                print meth
                if hasattr(a, "counter"):
                    a.counter += 1
                else:
                    a.counter = 1
                return meth(*args)
            cls.__dict__[a] = func

    return cls

@dec
class A:
    def debug(self):
        print "abcdef"


a = A()
a.debug()
print a.debug.counter

