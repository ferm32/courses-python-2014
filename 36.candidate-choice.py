class Candidate(object):

    def __init__(self, name="Unknown", age = 30, sex = "m", exp = 0, \
        test = 0, edu = "middle"):
            self.name = name
            self.age = age
            self.sex = sex
            self.exp = exp
            self.test = test
            self.edu = edu

    def get_age_weight(self):
        if self.age <= 20:
            return 1
        elif 21 <= self.age <= 30:
            return 0.3
        elif 31 <= self.age <= 40:
            return 0.5
        elif 41 <= self.age <= 50:
            return 0.3
        else:
            return 0

    def get_sex_weight(self):
        if self.sex == "m":
            return 0.6
        else:
            return 0.4

    def get_exp_weight(self):
        return self.exp / 30.0 if self.exp <=30 else 1

    def get_test_weigth(self):
        return self.test / 30.0

    def get_edu_weight(self):
        return 0.6 if self.edu == "high" else 0.4 if self.edu == "middle" else 0

    def criteria(self):
        return  0.25 * self.get_age_weight() + \
                0.20 * self.get_sex_weight() + \
                0.10 * self.get_exp_weight() + \
                0.30 * self.get_test_weigth() + \
                0.15 * self.get_edu_weight()

    def __lt__(self, otherman):
        if self.criteria() > otherman.criteria():
            return True
        else:
            return False

candidates = []
candidates.append(Candidate(name = "Vasiya", age = 25, sex = "m", exp = 3, \
    test = 23, edu ="high"))
candidates.append(Candidate("Olga", 45, "w", 8, 25, "high"))
candidates.append(Candidate("Petr", 65, "m", 2, 18, "middle"))
candidates.append(Candidate("Pavel", 18, "m", 0, 30, None))
candidates.append(Candidate("Irina", 37, "w", 11, 28, "middle"))

for i in xrange(len(candidates)):
    print "%10s, %2.4f" % (candidates[i].name, candidates[i].criteria())

candidates.sort(reverse = True)

print "Sorted (by criteria) candidates:"

for i in xrange(len(candidates)):
    print "%10s, %2.4f" % (candidates[i].name, candidates[i].criteria())
