import SocketServer, socket, threading

class MyRequestHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request.recv(1024)
        self.request.send(data)
        counter=0
        for line in data.split('\n'):
            counter += line.upper().count("PYTHON")
#        print("\"Python\" word count: ", counter)
        open('47.dz_socketserver.log', mode = "w").write("\"Python\" words count: %s\n" % (counter))
        return


address = ('', 8000)
server = SocketServer.TCPServer(address, MyRequestHandler)

t = threading.Thread(target=server.serve_forever)
t.start()
