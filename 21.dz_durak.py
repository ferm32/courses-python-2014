# coding: utf-8
# Прототип карточной игры Дурак
import random, sys, codecs, locale

sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)

class Cards_meta(object):
    suits = (u"♦", u"♥", u"♠", u"♣")
    ranks = ("2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A")

class Player(Cards_meta):
    def __init__(self, name):
	self.ordered_cards = []
	self.cards = []
	self.name = name
    def getonecard(self, deck):
	onecard = deck.getonecard()
	self.cards.append(onecard)
    def showmixed(self):
	print u"На руках у игрока "+self.name+":",
	for i in self.cards: print i,
	print ""

class Deck(Cards_meta):	# Колода
    ordered_deck = []	# Упорядоченная по мастям и по старшинству карт колода (двумерный список)
    _nonmixed_remains = []	# Перемешиваем колоду путём выемки случайных карт, это - остаток
    mixed_deck = []	# Перемешанная колода (однолинейный список)
    def __init__(self):
	for s in self.suits:
	    s_theleastcard = [s + self.ranks[0],]	# Младшая карта масти s
	    self.ordered_deck.append(s_theleastcard)
	    for r in self.ranks[1:]:
		s_nextcard = s + r
		self.ordered_deck[self.suits.index(s)].append(s_nextcard)
	self._nonmixed_remains = sum(self.ordered_deck, [])
	while self._nonmixed_remains != []:
	    newcard = self._nonmixed_remains.pop(self._nonmixed_remains.index(random.choice(self._nonmixed_remains)))
	    self.mixed_deck.append(newcard)
    def showordered(self):
	for i in xrange(4):
	    for j in xrange(13):
		print self.ordered_deck[i][j],
	    print ""
    def showmixed(self):
	for i in xrange(len(self.mixed_deck)):
		print self.mixed_deck[i],
	print ""
	print u"Перемешано карт: ", len(self.mixed_deck)
	print ""
    def getonecard(self):
	return self.mixed_deck.pop()

class Party(Cards_meta):
    players = []		# Игроки
    deck = []			# Колода
    trump = u""			# Козырь
    def __init__(self, number_of_players):
	for i in xrange(number_of_players):
	    newplayer = Player("Player"+str(i+1))
	    self.players.append(newplayer)
	self.deck = Deck()
    def dealcards(self):	# Раздать карты
	if len(self.deck.mixed_deck) == 52:	# Первая раздача, определяем козырь
	    print u"Пошла первая раздача."
	    trumpcard = self.deck.mixed_deck.pop(-1-len(self.players)*6)
	    self.trump = trumpcard
	    self.deck.mixed_deck.insert(0, trumpcard)
	    for i in xrange(6):
		for p in self.players:
		    if len(p.cards)<6:
			p.getonecard(self.deck)
    def showtrump(self):
	print u"Козырь: "+self.trump[0]+u" (карта: "+self.trump+")"

party = Party(2)
party.deck.showordered()
party.deck.showmixed()
party.dealcards()
party.showtrump()
print u"Колода после раздачи:"
party.deck.showmixed()
for p in party.players:
    p.showmixed()
