# encoding: utf-8
import os

def gen(dir):
    for filename in os.listdir(dir):
	print filename
	if filename.find(".txt") >= 0:
	    for line in open(dir+"/"+filename):
		yield line.count("Python")

for n in gen("testfolder"):
    print n

# Другой вариант, через объект генератора:
print "Другой вариант, через объект генератора:"

gen = (line.count("Python") for fi in os.listdir("testfolder")
     for line in open("testfolder"+"/"+fi) 
     if ".txt" in fi
     if "Python" in line)

for i in gen:
    print i
