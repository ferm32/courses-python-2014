ООП
===

"Классические" классы:
Class Py:
    pass

print Py

"Новые" классы:

class Py2(object):
    pass

==
class Utka:
    def krakat(self):
	print "Krya from", self
#Будет выведен адрес объекта self

#Как объект (self) будет выводиться на экран
    def __str__(self):
	return "Utka"

u = Utka()
u.kriakat()

isinstance(<object>, <class>)
isinstance(u, Utka)
True
class Gus(Utka)
    pass

isinstnce(u, Gus)
True

Взаимодействие объектов
-----------------------

a+b или
a.__add__(b)

Кроме одиночного наследования, есть множественное наследование:
class A(B, C)
    pass
# Им желательно не пользоваться, слишком сложно пользоваться

issubclass(Gus, Utka)
True
issubclass(Utka, Gus)
False

Сокрытие данных
---------------

.x
._x
.__x

class A:
    x = ... # Обычный атрибут
    _x = ... # Скрытый - чисто как пометка скрытности
    __x - ... # Ещё более скрытый, к нему нельзя обратиться как .__x

a.x
a._x
a.__A_x # Небольшая защита "от дурака"

Полиморфизм
-----------

def get_last(x):
    return x[-1]

print get_last([1,2,3])
print get_last("abcd")

def processor(reader, converter, writer):
    while True:
	data = reader.read()
	if not data: break
	data = converter(data)
	writer.write(data)

#reader, writer могут быть файлом open(''), open('', 'w')

Конструктор
-----------
    .__init__(self)

class Utka:
    def __init__(self, name)
	self.name = name

u = Utka('Alex')

Методы класса - это те же функции, но первый аргумент является self

class Gus(Utka):
    def __init__(self, name)
	self.peria="green"
	super(Gus, self).__init__(name)

Доступ к родителю:
super(<имя текущего класса>, self>)


Деструктор
----------

Пользоваться нежелательно, кроме отдельных случаев типа сокетов.
.__del__(self)

Имитация типов
--------------

class Plussable:
    def __add__(self, x): ...
    def __radd__(self, x): ...
    def __iadd__(self, x): ...

.__compare__ - сравнение
.__nonzero__ - является ли нулём
.__lt__ - <
.__le__ - <=
.__gt__ - >
.__ge__ - >=
.__eq__ - ==
.__ne__ - !=

Свойства для "новых" классов
----------------------------

x = property(getx, setx, delx, "I'm the 'x' property.")
y = property(gety, sety, dely, "I'm the 'y' property.")

class A(object):
    def setx(self, x):
	self._x = x
    def getx(self): return self._x
    def delx(self): del self._x

g = Gus()
a = A()
a.x = 7

Обработка атрибутов
-------------------

class AttDict(object):
    def __getattr__(self, name)
    def __setattr__(self, name, value)
    def __delattr__(self, name)

a = AttDict()
a.koeff = 0.7

class Auto(object):
    _col = None
    def setcol(self, col):
	self,_col = col
	self.cost -= 10000 # Машина после перекраски подешевела
    def getcol(self):
	return self._col
    def delcol(self):
	del self._col
    color = property(getcol, setcol, delcol, "I'm color")
# Задаёт строчку документации для getcol, setcol, delcol ?

a = Auto()
a.color = "green"
print a.cost
print "color:", a.color, "cost:", a.cost

ДЗ
реализовать на классах модель Карты
52 карты по значимости и 4 масти
действия - добрать руку, сходить (дурак)
концепция козырь
