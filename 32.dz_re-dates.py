# decoding: UTF-8
import re
# Ругулярное выражение для поиска в тексте дат формата дд.мм.гггг, в феврале допускается 29 дней, год произвольный.
regexp = r'(?:(?:0[1-9]|[12][0-9]|3[01])\.(?:01|03|05|07|08|10|12)|(?:0[1-9]|[12][0-9]|30)\.(?:04|06|09|11)|(?:0[1-9]|[12][0-9])\.02)\.\d{4}'
r = re.compile(regexp)
print re.findall(r, '29.02.2014 05.05.2014 30.02.1995 31.06.2023 00.01.2013 04.13.2033 32.12.2013 31.12.2013 27.04.2008 16.10.2007')

