# coding: utf-8

from argparse import ArgumentParser
from fractions import Fraction

parser = ArgumentParser(
            prog="Calculator",
            version="0.1",
            description="Descr", \
            usage="calculator.py operand1 action operand2, i.e. 1/2 - 1/6",
            add_help=True)

parser.add_argument("val1", help="Первый операнд", type=Fraction)
parser.add_argument("action", help="Действие (+ - * /)")
parser.add_argument("val2", help="Второй операнд", type=Fraction)
args = parser.parse_args()

print str(args.val1)+args.action+str(args.val2)+"="+ \
    str(args.val1 + args.val2) if args.action == "+" \
    else str(args.val1 - args.val2) if args.action == "-" \
    else str(args.val1 * args.val2) if args.action == "*" \
    else str(args.val1 / args.val2) if args.action == "/" \
    else "None"
