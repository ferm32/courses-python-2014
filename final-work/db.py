from sqlalchemy import create_engine, Table, MetaData, orm

#engine = create_engine('mysql+cymysql://root:@localhost/nachalka', echo=True)
engine = create_engine('mysql+oursql://root:@localhost/nachalka', echo=True)

#meta = MetaData(bind=engine, reflect=True)
meta = MetaData()
meta.reflect(engine, only=['mybb_forums', 'mybb_settings'])

class Forums_table(object):
    pass

class Settings_table(object):
    pass

orm.Mapper(Forums_table, meta.tables['mybb_forums'])
orm.Mapper(Settings_table, meta.tables['mybb_settings'])
session = orm.Session(bind=engine)

