#!/usr/local/bin/python3
#
# Snake Bulletin Board, the Pyhton's powered MyBB-alike forum engine
# ver 0.01 pre-alpha
#
# Copyright © 2014 ferm32. All rights reserved.
# Partially copyright 2005-2014 MyBB Group.
#

from sqlalchemy import create_engine, Table, MetaData, orm
from flask import Flask, url_for, redirect, render_template
from settings import settings
from db import session, Forums_table, Settings_table


def get_config_value(name):
    return session.query(Settings_table).filter(Settings_table.name==name).first().value

for n in ['bburl', 'bbname']:
    settings[n] = get_config_value(n)

app = Flask(__name__)
app.config.from_pyfile('sbb.cfg')


@app.route('/')
def display_index():
    return render_template(
        'index.html',
        settings = settings,
        forums = session.query(Forums_table).all()
        )

@app.route('/forum_<fid>')
def display_forum(fid):
    return render_template(
        'forumdisplay.html',
        settings = settings
        )


if __name__ == '__main__':
    app.run()

